# Final Projects

**Details of the checkpoints may be revised until they are assigned on T-Square.**

There are five problems to choose as project topics:

* Two problems with dense calculations:

    * [Discrete Fourier transform](Discrete-Fourier-Transform/)
    * [Matrix-matrix multiply-add](Matrix-Matrix-Multiply-Add/)

* Two problems with structured calculations:

    * [Lattice Boltzmann method](Lattice-Boltzmann-Method/)
    * [Gauss-Seidel iteration](Gauss-Seidel-Iteration/)

* One problem with unstructured calculations:

    * [Breadth-first search](Breadth-First-Search/)

## Proper Conduct 

* Teams may discuss their work with each other.
* Algorithms / results that you did not derive must be cited.
* All writing and code must be original:
    * External libraries like BLAS are prohibited without special permission.

## Checkpoint 0: Choose Your Project & Team

**Due: Friday, September 8**

* You may work in pairs if you'd like.

## Checkpoint 1: Serial Analysis & Implementation

Report:

* Discuss applications to which your kernel is relevant.
* Give asymptotic serial analysis (RAM model):
    * What is the best asymptotic performance ("Big-O") of existing serial algorithms?
    * What is the theoretical best asymptotic performance?
    * What is the asymptotic performance of your code?

Code:

* Write a valid serial code that performs your calculation.
* It must conform to an API that we will specify for each project choice.
* We will have a driver for running each code: your primary goal is correctness.

## Checkpoint 2: Parallel Analysis & Machine Choice

Report:

* Choose a computing platform for your parallel implementation:
    * It must be a platform that instructors/graders have access to.
    * If you're already thinking about something exotic, see us soon.
* Discuss the characteristics of the machine that will be most relevant to the
  performance of your code.
* Choose a parallel computing model that approximates your platform.
* Give pseudocode for your algorithm on your chosen model.
* Give asymptotic analysis including the relevant model parameters.

## Checkpoint 3: Parallel Implementation

Code:

* Write a valid parallel code that performs your calculation.
* It must conform to an API that we will specify for each project choice (modulo data layout).
* We will have a driver for running each code: your primary goal is correctness.
* Verify that instructors/graders can run the driver on your platform.

## Final: Parallel Optimization

Code:

* Submit your best performing code.

Report:

* Describe what optimizations you made and why.
* Justify your performance: report performance statistics that support that
  your *implementation* matches the performance predicted by your computing model.
